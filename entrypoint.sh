#!/bin/bash
if [ -e /opt/gopasswd.properties ]
then
    echo "Senhas do servidor já definidas"
else
    htpasswd -cb -B /opt/gopasswd.properties $USER $PASSWORD
fi

mkdir -p /var/lib/go-server/run/
mkdir -p /var/lib/go-server/plugins/external/
mv /opt/google-oauth-authorization-plugin-3.0.1-28.jar /var/lib/go-server/plugins/external/
mv /opt/docker-elastic-agents-3.0.0-245.jar /var/lib/go-server/plugins/external/
chown go:go -R /var/lib/go-server

ssh-keygen -F bitbucket.org

if [ $? -ne 0 ]
then
    mkdir -p /var/go/.ssh
    touch /var/go/.ssh/known_hosts
    ssh-keyscan bitbucket.org >> /var/go/.ssh/known_hosts
    chown -R go:go /var/go/.ssh
    chmod 600 /var/go/.ssh/known_hosts
fi

/usr/share/go-server/bin/go-server console