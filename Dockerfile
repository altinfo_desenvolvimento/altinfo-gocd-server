FROM fedora

LABEL maitainer="Luiz Fernando Pereira <luizfernandopereira@outlook.com.br>" \
      company="Alternativa Informática"

RUN dnf install httpd-tools git -y \
    && dnf install https://download.gocd.org/binaries/20.10.0-12356/rpm/go-server-20.10.0-12356.noarch.rpm -y \
    && dnf install https://download.docker.com/linux/fedora/33/x86_64/stable/Packages/docker-ce-cli-20.10.1-3.fc33.x86_64.rpm -y \
    && dnf clean all

COPY entrypoint.sh /opt/entrypoint.sh
COPY plugins/google-oauth-authorization-plugin-3.0.1-28.jar /opt/google-oauth-authorization-plugin-3.0.1-28.jar
COPY plugins/docker-elastic-agents-3.0.0-245.jar /opt/docker-elastic-agents-3.0.0-245.jar
RUN chmod +x /opt/entrypoint.sh

EXPOSE 8153 8154

ENTRYPOINT [ "/opt/entrypoint.sh" ]